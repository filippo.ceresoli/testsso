#!/bin/bash

if [ "${SCRIPT_DEBUG}" = "true" ] ; then
    set -x
    echo "Script debugging is enabled, allowing bash commands and their arguments to be printed as they are executed"
fi
echo "INTO CUSTOM SCRIPT"
injected_dir=$1
source /usr/local/s2i/install-common.sh
install_modules ${injected_dir}/modules
configure_drivers ${injected_dir}/drivers.env
## TO REMOVE FROM PRODUCTION?????? ##
rm -fr $JBOSS_HOME/standalone/configuration/standalone_xml_history/current
echo "*************************** dump ***********************************"
cat $JBOSS_HOME/standalone/configuration/standalone-openshift.xml
echo "*************************** dump ***********************************"

